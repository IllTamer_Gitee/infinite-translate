# InfiniteTranslate 帮助文档
## 1.0.X
### 使用相关
> 注意: 默认翻译源语言可自动检测, 默认目标语言为中文
#### 使用方式  
  请先在软件根目录下 InfiniteTranslate/config.yml 文件中配置您的百度翻译 
  api 的 appId 与密匙, 后在 InfiniteTranslate/source 中放入您需要翻译的文件, 
  运行程序按提示操作即可

#### 默认支持文件种类  
- **txt**  
  对于非中文的语言, 默认以 **.** (\u002e) 作为语句间的分隔符, 
  故您需保证提供的源文件的符号形式, 以便程序进行合理的解析.
- **json**  
  我们认为以 json 格式封装的数据仅包含消息内容本身, 故对于全部内容进行翻译处理
- **yaml**  
  对于 yaml 这一格式, 软件提供了最强大的支持 ! 程序会对可能需要被翻译的部分进行判断并
  将其翻译, 整个过程不会破坏 yaml 的原有结构 (**甚至是顺序**). 唯一不足的是, 在这个版本
  yaml 在翻译后将无法保留注释, 我会在后续的更新中解决这个问题
### 开发者相关
  如果您要对程序支持翻译的文件种类进行对应的拓展, 请按照以下方式进行解析器的注册
```java
// 注册支持解析的文件后缀名种类
FileType#addSupportFileType(String, String[])

// 实现 me.illtamer.infinite.file.parse.TypeParse 接口
public class MyTypeParse implements TypeParser {
  // 调用接口进行文件内容翻译与输出
  void parse(TranslatorAdaptor adaptor, FileSource source) throws Exception;
  // 复写 parserType 方法返回解析器需要解析的文件类型
  FileType parserType() {
      return FileType.getFileType(String);
  }
}
```
## 后续更新计划
- 支持网络文件翻译并保存到本地 !
- 支持基于 google 的翻译 api !
- 可视化的图形编辑界面 !
- yaml 保留并翻译注释 !