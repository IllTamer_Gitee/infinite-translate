package me.illtamer.infinite;

import lombok.extern.slf4j.Slf4j;
import me.illtamer.infinite.config.Configuration;
import me.illtamer.infinite.file.loader.FileAlterationManager;
import me.illtamer.infinite.file.loader.FileManager;
import me.illtamer.infinite.file.loader.SourceFileHandler;
import me.illtamer.infinite.file.parser.JsonParser;
import me.illtamer.infinite.file.parser.TextParser;
import me.illtamer.infinite.file.parser.YamlParser;
import me.illtamer.infinite.hook.TranslateHook;
import me.illtamer.infinite.hook.realize.BaiDuTranslator;

import java.nio.file.StandardWatchEventKinds;

/**
 * 基于网站 api
 * https://fanyi.baidu.com/#
 *      输入语言 -> en /
 *      目标语言 -> zh /
 *      URLEncoder String -> Hello%20World
 * */

@Slf4j
public class Translate {

    public static final Configuration CONFIG = new Configuration("config.yml");

    public static void main(String[] args) throws Exception {
        // 注册监听文件夹
        FileAlterationManager.register(Configuration.SOURCE_FOLDER.getAbsolutePath(), new SourceFileHandler(), StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);
//        FileAlterationManager.register(Configuration.TARGET_FOLDER.getAbsolutePath(), new SourceFileHandler(), StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE);

        // 注册文件处理器
        FileManager.addFileParse(new YamlParser());
        FileManager.addFileParse(new JsonParser());
        FileManager.addFileParse(new TextParser());

        // 注册翻译适配器
        TranslateHook.register("baidu", new BaiDuTranslator());

//        // 注册拓展文件后缀
//        FileType.addSupportFileType();

        // 加载 source 文件夹内文件
        FileManager.loadSourceFiles();


        TranslateController.newInstance();
//        FileAlterationManager.unregisterAll();
    }

}
