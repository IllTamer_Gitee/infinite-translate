package me.illtamer.infinite.exception;

import me.illtamer.infinite.hook.TranslatorAdaptor;

public class InitializationException extends RuntimeException {

    public InitializationException(String message, TranslatorAdaptor adaptor) {
        super(getAdaptorMessage(message, adaptor));
    }

    public InitializationException(String message, TranslatorAdaptor adaptor, Throwable cause) {
        super(getAdaptorMessage(message, adaptor), cause);
    }

    private static String getAdaptorMessage(String message, TranslatorAdaptor adaptor) {
        return "There are some exception occurred when initializing " + adaptor.getClass().getCanonicalName() + ": " + message;
    }

}
