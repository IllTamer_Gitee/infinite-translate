package me.illtamer.infinite.exception;

public class LengthException extends RuntimeException {

    private static final String MESSAGE = "Length out of range: ";

    public LengthException() {
    }

    public LengthException(int length) {
        super(MESSAGE + length);
    }

    public LengthException(int length, Throwable cause) {
        super(MESSAGE + length, cause);
    }

}
