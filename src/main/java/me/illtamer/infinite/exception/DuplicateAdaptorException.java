package me.illtamer.infinite.exception;

public class DuplicateAdaptorException extends RuntimeException {

    public DuplicateAdaptorException(String message) {
        super(message);
    }

    public DuplicateAdaptorException(String message, Throwable cause) {
        super(message, cause);
    }

}
