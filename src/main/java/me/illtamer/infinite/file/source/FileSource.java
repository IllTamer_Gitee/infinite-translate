package me.illtamer.infinite.file.source;

import me.illtamer.infinite.file.FileType;

/**
 * 文件源接口
 * */
public interface FileSource {

    /**
     * 获取文件内容
     * */
    String getContent();

    /**
     * 获取文件名称
     * */
    String getName();

    /**
     * 获取文件后缀名
     * @apiNote 根据返回的类型分发文件
     * @return {@link FileType}
     * */
    FileType getType();

}
