package me.illtamer.infinite.file.source;

import me.illtamer.infinite.file.FileType;
import me.illtamer.infinite.util.FileUtil;

import java.io.File;

/**
 * 本地文件源
 * */
public class LocalFileSource implements FileSource {

    private final File localFile;
    private FileType type;
    private String content;

    public LocalFileSource(File localFile) {
        this.localFile = localFile;
    }

    @Override
    public String getContent() {
        if (content != null) return content;
        return content = FileUtil.getFileContent(localFile);
    }

    @Override
    public FileType getType() {
        if (type != null) return type;
        return type = FileType.getFileType(localFile);
    }

    @Override
    public String getName() {
        return localFile.getName();
    }

}
