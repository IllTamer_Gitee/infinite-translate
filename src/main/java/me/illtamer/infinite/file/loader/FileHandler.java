package me.illtamer.infinite.file.loader;

import java.io.File;
import java.nio.file.WatchEvent;

/**
 * 文件监听处理接口
 * */
public interface FileHandler {

    /**
     * 事件处理方法
     * @param eventType 事件种类
     * @param file 触发事件的文件对象，可能不存在
     * @apiNote 触发对应事件时被调用
     * */
    void handle(WatchEvent.Kind<?> eventType, File file);

}
