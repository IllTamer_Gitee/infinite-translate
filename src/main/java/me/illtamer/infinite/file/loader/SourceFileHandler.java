package me.illtamer.infinite.file.loader;

import lombok.extern.slf4j.Slf4j;
import me.illtamer.infinite.TranslateController;
import me.illtamer.infinite.factory.SingletonFactory;

import java.io.File;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;

/**
 * source 文件夹监听器
 * */
@Slf4j
public class SourceFileHandler implements FileHandler {

    @Override
    public void handle(WatchEvent.Kind<?> eventType, File file) {
        String name = file.getName();
        String parentPath = file.getAbsolutePath().substring(0, file.getAbsolutePath().length() - name.length());
        if (eventType == StandardWatchEventKinds.ENTRY_CREATE) {
            FileManager.WAITING_FILES.put(name, file);
            log.debug("Waiting file list({}) added({})", parentPath, name);
            SingletonFactory.getSingleton(TranslateController.class).refresh();
        } else if (eventType == StandardWatchEventKinds.ENTRY_DELETE) {
            FileManager.WAITING_FILES.remove(name);
            log.debug("Waiting file list({}) removed({})", parentPath, name);
            SingletonFactory.getSingleton(TranslateController.class).refresh();
        }
    }

}
