package me.illtamer.infinite.file.parser;

import me.illtamer.infinite.file.FileType;
import me.illtamer.infinite.file.source.FileSource;
import me.illtamer.infinite.hook.TranslatorAdaptor;

/**
 * 类型处理器接口
 * */
public interface TypeParser {

    /**
     * 文件翻译处理方法
     * */
    void parse(TranslatorAdaptor adaptor, FileSource source) throws Exception;

    /**
     * 类型处理器处理的文件种类
     * */
    default FileType parserType() {
        return FileType.ALL;
    }

}
