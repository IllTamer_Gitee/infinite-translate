package me.illtamer.infinite.pojo;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class TransObject {

    /**
     * 原始语言
     * 后续封装为枚举类
     * */
    private final String from;

    /**
     * 目标语言
     * */
    private final String to;

    /**
     * 结果集
     * @apiNote {
     *      src1: dst1,
     *      src2: dst2
     * }
     * */
    private final List<TransPair> transPairs;

    /**
     * 响应状态码
     * */
    private final Response response;

    @Data
    public static class TransPair {

        /**
         * 原文
         * */
        private final String src;

        /**
         * 译文
         * */
        private final String dst;

    }

}
