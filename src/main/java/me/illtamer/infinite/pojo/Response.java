package me.illtamer.infinite.pojo;

/**
 * 解决方案
 * https://api.fanyi.baidu.com/product/113
 * */
public enum Response {

    SUCCESS(52000, "成功"),

    TIMEOUT(52001, "请求超时"),

    SERVER_ERROR(52002, "系统错误"),

    UNAUTHORIZED_USER(52003, "未授权用户"),

    NULL_PARAM(54000, "必填参数为空"),

    INVALID_SIGN(54001, "签名错误"),

    LIMITED_ACCESS(54003, "访问频率受限"),

    NO_MONEY(54004, "账户余额不足"),

    LONG_QUERY_LIMITED(54005, "长query请求频繁"),

    ILLEGAL_IP(58000, "客户端IP非法"),

    UNKNOWN_TO(58001, "译文语言方向不支持"),

    SERVER_CLOSE(58002, "服务当前已关闭"),

    NOT_EFFECTIVE(90107, "认证未通过或未生效"),

    UNKNOWN_ERROR(-1, "未知错误");

    Response(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private final int code;
    private final String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return msg;
    }

    public static Response get(int code) {
        if (code == SUCCESS.code) {
            return SUCCESS;
        } else if (code == TIMEOUT.code) {
            return TIMEOUT;
        } else if (code == SERVER_ERROR.code) {
            return SERVER_ERROR;
        } else if (code == UNAUTHORIZED_USER.code) {
            return UNAUTHORIZED_USER;
        } else if (code == NULL_PARAM.code) {
            return NULL_PARAM;
        } else if (code == INVALID_SIGN.code) {
            return INVALID_SIGN;
        } else if (code == LIMITED_ACCESS.code) {
            return LIMITED_ACCESS;
        } else if (code == NO_MONEY.code) {
            return NO_MONEY;
        } else if (code == LONG_QUERY_LIMITED.code) {
            return LONG_QUERY_LIMITED;
        } else if (code == ILLEGAL_IP.code) {
            return ILLEGAL_IP;
        } else if (code == UNKNOWN_TO.code) {
            return UNKNOWN_TO;
        } else if (code == SERVER_CLOSE.code) {
            return SERVER_CLOSE;
        } else if (code == NOT_EFFECTIVE.code) {
            return NOT_EFFECTIVE;
        } else {
            return UNKNOWN_ERROR;
        }
    }
}
