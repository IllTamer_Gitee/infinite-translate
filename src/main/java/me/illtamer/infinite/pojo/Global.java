package me.illtamer.infinite.pojo;

import me.illtamer.infinite.Translate;
import me.illtamer.infinite.config.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.util.HashMap;
import java.util.Map;

public class Global {

    private static final Map<String, String> HEADERS = new HashMap<>();

    private final Configuration config = Translate.CONFIG;

    static {
        HEADERS.put("Content-Type", "application/x-www-form-urlencoded");
        HEADERS.put("Connection", "keep-alive");
        HEADERS.put("Mimetype", "application/json");
        HEADERS.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36");
    }

    public Connection createConnection(String url) {
        ConfigurationSection httpConfig = config.getSection("http-config");
        return Jsoup.connect(url)
                .headers(HEADERS)
                .timeout(httpConfig.getInt("timeout"))
                .ignoreContentType(true);
    }

    public String getEnabledApi() {
        return config.get("enabled", String.class);
    }

    public long getMaxTaskCost() {
        return config.getConfig().getInt("max-task-cost");
    }

}
