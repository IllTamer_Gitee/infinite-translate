package me.illtamer.infinite.hook;

import me.illtamer.infinite.pojo.Global;
import org.bukkit.configuration.ConfigurationSection;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

/**
 * 翻译通用接口
 * */
public interface TranslatorAdaptor {

    /**
     * 初始化方法
     * @param global 全局域对象
     * @param apiSection api-manager 节点下的全部配置片段
     *      {@link ConfigurationSection}
     * @return 初始化是否成功
     * */
    boolean init(@NotNull Global global, ConfigurationSection apiSection);

    /**
     * 翻译方法
     * @param src 原始字段
     * @param from 原始语言
     * @param to 目标语言
     * */
    @NotNull
    String translate(@NotNull String src, @NotNull String from, @NotNull String to) throws IOException;

    /**
     * 一次翻译所支持的最大字符长度 (url编码)
     * @apiNote 如果不进行重写，则默认不限制传入的内容长度
     * */
    default int getMaxLength() {
        return -1;
    }

}
