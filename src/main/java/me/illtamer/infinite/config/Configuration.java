package me.illtamer.infinite.config;

import org.bukkit.configuration.ConfigurationSection;

import java.io.File;

public class Configuration extends ConfigLoader {

    public static final File DATA_FOLDER = new File(System.getProperty("user.dir") + "/InfiniteTranslate");
    public static final File SOURCE_FOLDER = new File(DATA_FOLDER, "source");
    public static final File TARGET_FOLDER = new File(DATA_FOLDER, "target");

    static {
        if (!SOURCE_FOLDER.exists()) SOURCE_FOLDER.mkdirs();
        if (!TARGET_FOLDER.exists()) TARGET_FOLDER.mkdirs();
    }

    public Configuration(String name) {
        super(DATA_FOLDER, name);
    }

    /**
     * 获取配置文件片段
     * @param path 文件路径坐标，用 . 链接
     * */
    public ConfigurationSection getSection(String path) {
        return getConfig().getConfigurationSection(path);
    }

    /**
     * 从指定路径获取配置参数
     * @param path 文件路径坐标，用 . 链接
     * @param clazz 属性类型
     * */
    public <T> T get(String path, Class<T> clazz) {
        return getConfig().getObject(path, clazz);
    }

}
